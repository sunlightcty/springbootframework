package scau.cty.boot.service;

import scau.cty.boot.entity.RolePermission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
public interface RolePermissionService extends IService<RolePermission> {
	
}
