package scau.cty.boot.service.impl;

import scau.cty.boot.entity.UserRole;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import scau.cty.boot.mapper.UserRoleMapper;
import scau.cty.boot.service.UserRoleService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Service
public class RoleUserServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {
	
}
