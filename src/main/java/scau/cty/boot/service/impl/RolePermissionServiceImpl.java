package scau.cty.boot.service.impl;

import scau.cty.boot.entity.RolePermission;
import scau.cty.boot.mapper.RolePermissionMapper;
import scau.cty.boot.service.RolePermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {
	
}
