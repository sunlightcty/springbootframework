package scau.cty.boot.service.impl;

import scau.cty.boot.entity.Permission;
import scau.cty.boot.mapper.PermissionMapper;
import scau.cty.boot.service.PermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {
	
}
