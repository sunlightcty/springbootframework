package scau.cty.boot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scau.cty.boot.entity.User;
import scau.cty.boot.mapper.UserMapper;
import scau.cty.boot.service.UserService;

import java.util.Date;
import java.util.List;

/**
 * Created by SunlightCloud on 2017/7/19.
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{

    @Autowired
    private UserMapper userMapper;


    @Override
    public void create(String name, Integer age) {
    }

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public int insert(String id, String username, String password, String salt, Date date) {
        return userMapper.insert(id,username,password,salt,date);
    }


}
