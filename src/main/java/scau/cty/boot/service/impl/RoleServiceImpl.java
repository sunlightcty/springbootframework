package scau.cty.boot.service.impl;

import scau.cty.boot.entity.Role;
import scau.cty.boot.mapper.RoleMapper;
import scau.cty.boot.service.RoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
	
}
