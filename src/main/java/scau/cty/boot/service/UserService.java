package scau.cty.boot.service;

import com.baomidou.mybatisplus.service.IService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import scau.cty.boot.entity.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/7/19.
 */
@CacheConfig(cacheNames = "users")
public interface UserService extends IService<User>{
    /**
     * 新增一个用户
     *
     * @param name
     * @param age
     */
    @Cacheable  //返回值将会被缓存
    @CachePut
    //用于更新 删除 修改操作
    void create(String name, Integer age);


    User findByUsername( String username);

    int insert(String id
                ,String username
                ,String password
                ,String salt
                ,Date date);

}
