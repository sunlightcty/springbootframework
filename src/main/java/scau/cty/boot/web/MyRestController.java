package scau.cty.boot.web;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/18.
 */
@RestController
@RequestMapping(value="/users")
public class MyRestController {

    @GetMapping(value="/{user}")
    public String getUser(@PathVariable Long user) {
        // ...
        return "/users/{user} ---> successful";
    }

    @GetMapping(value="/{user}/customers")
    List<String> getUserCustomers(@PathVariable Long user) {
        // ...
        List<String> arr = new ArrayList<>();
        arr.add("/users/{user}/customers----> successful");
        return arr;
    }

    @DeleteMapping(value="/{user}")
    public String deleteUser(@PathVariable Long user) {
        // ...
        return "";
    }

}