package scau.cty.boot.task;

import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务具体实现类
 * 需在主类用注解开启定时任务配置
 * Created by SunlighCloud on 2017/7/19.
 */
public class ScheduledTasks {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    /**
        @Scheduled (fixedRate = 5000) ：上一次开始执行时间点之后5秒再执行
        @Scheduled (fixedDelay = 5000) ：上一次执行完毕时间点之后5秒再执行
        @Scheduled (initialDelay=1000, fixedRate=5000) ：第一次延迟1秒后执行，之后按fixedRate的规则每5秒执行一次
        @Scheduled (cron="*5 * * * * *") ：通过cron表达式定义规则
     */
    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        System.out.println("现在时间：" + dateFormat.format(new Date()));
    }
}
