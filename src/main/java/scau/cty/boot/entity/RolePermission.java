package scau.cty.boot.entity;

import scau.cty.base.entity.SuperEntity;

/**
 * Created by SunlightCloud on 2017/7/21.
 */
public class RolePermission extends SuperEntity<RolePermission>{
    private Long roleId;
    private Long permissionId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }
}
