package scau.cty.boot.entity;

import scau.cty.base.entity.SuperEntity;

/**
 * Created by SunlightCloud on 2017/7/21.
 */
public class Permission extends SuperEntity<Permission>{
    private String permissionName;
    private String permissionMessage;

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPermissionMessage() {
        return permissionMessage;
    }

    public void setPermissionMessage(String permissionMessage) {
        this.permissionMessage = permissionMessage;
    }
}
