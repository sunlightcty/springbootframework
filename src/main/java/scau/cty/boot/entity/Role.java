package scau.cty.boot.entity;

import scau.cty.base.entity.SuperEntity;

/**
 * Created by SunlightCloud on 2017/7/19.
 */
public class Role extends SuperEntity<Role>{
    private String roleName;
    private String roleMessage;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleMessage() {
        return roleMessage;
    }

    public void setRoleMessage(String roleMessage) {
        this.roleMessage = roleMessage;
    }
}
