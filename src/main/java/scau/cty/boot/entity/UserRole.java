package scau.cty.boot.entity;

import scau.cty.base.entity.SuperEntity;

/**
 * Created by SunlightCloud on 2017/7/21.
 */
public class UserRole extends SuperEntity<UserRole>{
    private Long userId;
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
