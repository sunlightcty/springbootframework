package scau.cty.boot.entity;

import org.springframework.data.annotation.Transient;
import scau.cty.base.entity.SuperEntity;

import java.util.Date;

/**
 * Created by Administrator on 2017/7/19.
 */
public class User extends SuperEntity<User>{

    private String username;
    private String password;
    private String salt;
    private Date create_time;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    //shiro需要
    public String getCredentialsSalt() {
        return username+salt;
    }
}
