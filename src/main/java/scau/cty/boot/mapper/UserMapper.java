package scau.cty.boot.mapper;

import org.apache.ibatis.annotations.*;
import scau.cty.base.mybatis.SuperMapper;
import scau.cty.boot.entity.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/7/19.
 */
@Mapper
public interface UserMapper extends SuperMapper<User>{

    /**
     * 可以采取注解的方式 也可以采取xml配置方式
     */

    @Select("SELECT * FROM USER WHERE username = #{username}")
    User findByUsername(@Param("username") String username);

    @Insert("INSERT INTO USER(id, username, password, salt, create_time) VALUES(#{id}, #{username}," +
            " #{password}, #{salt}, #{create_time})")
    int insert(@Param("id") String id,
               @Param("username") String username,
               @Param("password") String password,
               @Param("salt") String salt,
               @Param("create_time")Date date);
}
