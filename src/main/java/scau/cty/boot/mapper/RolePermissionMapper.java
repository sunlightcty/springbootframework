package scau.cty.boot.mapper;

import org.apache.ibatis.annotations.Mapper;
import scau.cty.base.mybatis.SuperMapper;
import scau.cty.boot.entity.RolePermission;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Mapper
public interface RolePermissionMapper extends SuperMapper<RolePermission> {

}