package scau.cty.boot.mapper;

import org.apache.ibatis.annotations.Mapper;
import scau.cty.base.mybatis.SuperMapper;
import scau.cty.boot.entity.UserRole;


/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author SunlightCloud
 * @since 2017-07-21
 */
@Mapper
public interface UserRoleMapper extends SuperMapper<UserRole> {

}