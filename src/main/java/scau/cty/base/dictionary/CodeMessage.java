package scau.cty.base.dictionary;

/**
 * Created by SunlightCloud on 2017/7/21.
 */
public enum CodeMessage {

    /**
     * 通用返回信息
     */
    OPERATION_SUCCESS("操作成功"),

    OPERATION_FAILED("操作失败"),

    ILLEGAL_ARGUMENT("请求参数不合法"),

    UNEXIT_ACCOUNT("用户名不存在"),

    ERROR_PASSWORD("密码错误"),

    REJECT_ACCESS("拒绝访问"),

    SYSTEM_EXCEPTION("系统异常"),

    SYSTEM_BUSY("系统繁忙");

    /**
     * 业务扩展
     */
    // ....


    private String msg;
    CodeMessage(String msg){
        this.msg = msg;
    }
}
