package scau.cty.base.transactional;

import org.springframework.transaction.annotation.Transactional;

/**
 * 事务管理
 * Created by SunlightCloud on 2017/7/19.
 */
public class TransactionalManager {

    /**
     * 加上@Transactional 注解使得本方法存在事务管理
     * @throws Exception
     */
    @Transactional
    public void insertBatch() throws Exception{
        /*
            批量插入数据 中间发生异常
         */
    }
}
