package scau.cty.base.util;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * 手机验证码实现
 * Created by SunlightCloud on 2017/3/26.
 */
public class PhoneKeyUtil {

    private static final String url = "https://eco.taobao.com/router/rest"; //阿里大鱼平台 固定
    private static final String app_key = "23566089"; //企业申请应用的appkey 由阿里大鱼分配
    private static final String secret = "9fc074673e4d6f30445c6ad9801ee39f"; //对应应用的secret 由阿里大鱼分配
    private static final String smsType = "normal"; //短信类型 目前只有一种normal一种选择
    private static final String smsFreeSignName1 = "云上"; // 企业的签名 可设置多个 后台自己选择

    /**
     * 发送手机验证码
     * @param username 请求用户的名字 可为空  方便以后的需求 这里要求必须传值
     * @param phoneNumber 手机号码 必填
     * @param smsParmString 短信需要传进的参数 ，先参考个人在阿里大鱼设置的短信模板
     * @param smsTemplateCode 短信模板的编号  可多个 在业务逻辑处进行选择
     */

    public static Integer sendPhoneKeyUtils(String username, String phoneNumber, String smsParmString, String smsTemplateCode) {
        Integer code ;//标识
        TaobaoClient client = new DefaultTaobaoClient(url, app_key, secret); //联接到阿里大鱼
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest(); //请求对象
        req.setExtend(username);// 请求会员信息 选填
        req.setSmsType(smsType);  //短信类型  填normal 必填
        req.setSmsFreeSignName(smsFreeSignName1); //签名 必填
        req.setSmsParamString(smsParmString);// 短信内容 选填
        req.setRecNum(phoneNumber);  //电话号码 demo 必填
        req.setSmsTemplateCode(smsTemplateCode); //短信模板id 必填  SMS_58050195 JAVA课程设计
        AlibabaAliqinFcSmsNumSendResponse rsp = null;  //响应对象
        try {
            rsp = client.execute(req);
            code = 1;
        } catch (ApiException e) {
            code = 0;
            e.printStackTrace();
        }
        System.out.println(rsp.getBody()); //打印响应信息
        return code;
    }

    /**
     * 获取随机四位数验证码
     * @return
     */
    public static String getRandomNumber() {
        String phoneKey;
        phoneKey = "" + (int) (Math.random() * 10) + (int) (Math.random() * 10) + (int) (Math.random() * 10) + (int) (Math.random() * 10);
        return phoneKey;
    }

}
