package scau.cty.base.config.swagger2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置类
 * 利用Swagger2 生成RESTful API
 * Created by SunlighCloud on 2017/7/19.
 */
@Configuration //通过这个注解 使得Spring加载本类配置
@EnableSwagger2 //启用Swagger2
public class Swagger2 {
    /**
     * 创建Docket对象
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())  //创建API基本信息
                .select() //控制暴露那些接口给Swagger展现
                .apis(RequestHandlerSelectors.basePackage("scau.cty.boot.web")) //选中Web层 即Controller 可用@APiIgnore避免扫描
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot中使用Swagger2构建RESTful APIs")
                .description("项目API")
                .termsOfServiceUrl("URL配置")
                .contact("SunlightCloud")
                .version("1.0（版本控制）")
                .build();
    }
}
