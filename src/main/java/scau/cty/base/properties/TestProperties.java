package scau.cty.base.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 通过配置文件加载自己自定义的属性值
 * Created by SunlightCloud on 2017/7/19.
 */
@Component
public class TestProperties {

    @Value("${scau.cty.name}")
    private String name;
    @Value("${scau.cty.title}")
    private String title;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
