package scau.cty.base.web;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.LogManager;

/**
 * shiro 登录登出
 * Created by SunlightCloud on 2017/7/21.
 */
@Controller
public class LoginController {

    private org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger(getClass());


    @GetMapping("/login")
    protected String doLogin(String username, String password ){
        SecurityUtils.getSecurityManager().logout(SecurityUtils.getSubject());
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        org.apache.shiro.subject.Subject subject = SecurityUtils.getSubject();
        token.setRememberMe(true);
        try {
            subject.login(token);
        } catch (UnknownAccountException ex) {
            logger.info("-----验证失败，用户不存在-----");
            return "error";
        } catch (IncorrectCredentialsException ex) {
            logger.info("-----验证失败，账号或密码错误-----");
            return "error";
        }
        return "authtest";
    }

    @GetMapping("/logout")
    protected String doLogout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "authtest";
    }

}
