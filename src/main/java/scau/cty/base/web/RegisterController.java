package scau.cty.base.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import scau.cty.base.util.PasswordHelper;
import scau.cty.boot.entity.User;
import scau.cty.boot.service.UserService;

import java.util.Date;

/**
 * Created by SunlightCloud on 2017/7/24.
 */
@Controller
public class RegisterController extends BaseController {

    @Autowired
    private UserService userService;

    @GetMapping("/go_register")
    public String goRegist(){
        return "register";
    }

    @GetMapping("/register")
    public String doRegist(User user) {
        user.setCreate_time(new Date());
        PasswordHelper.encryptPassword(user);
        userService.insert(user);
        return "redirect:index";
    }

}
