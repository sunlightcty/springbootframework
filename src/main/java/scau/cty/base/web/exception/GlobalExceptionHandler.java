package scau.cty.base.web.exception;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.shiro.authz.UnauthorizedException;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import scau.cty.base.dictionary.CodeMessage;
import scau.cty.base.web.BaseController;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 全局异常处理类
 * Created by SunlightCloud on 2017/7/19.
 */
@ControllerAdvice
public class GlobalExceptionHandler extends BaseController{

    private org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger(getClass());

    public static final String DEFAULT_ERROR_VIEW = "error";


    @ExceptionHandler(value = {UnauthorizedException.class})
    @ResponseBody
    public String catchUnauthorizedException(UnauthorizedException ex) {
        logger.info(ex);
        return createErrorJson(CodeMessage.REJECT_ACCESS);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseBody
    public String catchRuntimeException(RuntimeException ex) {
        logger.info(ex);
        return createSystemErrorJson(CodeMessage.SYSTEM_EXCEPTION);
    }

}
