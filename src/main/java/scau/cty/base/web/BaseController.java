package scau.cty.base.web;

import com.alibaba.fastjson.JSON;
import com.sun.tools.javac.jvm.Code;
import scau.cty.base.dictionary.CodeMessage;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by SunlightCloud on 2017/7/21.
 */
public class BaseController {

    private static final String OPERATION_SUCCESS = "0";

    private static final String OPERATION_FAILED = "1";

    protected static final String SYSTEM_PROBLEM = "9";

    /**
     * map封装基本状态信息
     *
     */
    private Map putSuccessInMap() {
        Map map = new LinkedHashMap<>();
        map.put("statusCode",OPERATION_SUCCESS);
        map.put("statusMsg", CodeMessage.OPERATION_SUCCESS);
        return map;
    }
    private Map putFailedInMap() {
        Map map = new LinkedHashMap<>();
        map.put("statusCode",OPERATION_FAILED);
        map.put("statusMsg", CodeMessage.OPERATION_FAILED);
        return map;
    }


    /**
     *  更新、删除操作类返回成功方法
     * @return
     */
    protected String createSuccessJson() {
        return JSON.toJSONString(putSuccessInMap());
    }

    /**
     * 返回指定单个对象内容
     * @param objectName
     * @param object
     * @return
     */
    protected String createSuccessJson(String objectName, Object object) {
        return JSON.toJSONString(putSuccessInMap().put(objectName, object));
    }

    /**
     * 返回Map 内容
     */

    protected String createSuccessJson(Map map) {
        map.put("statusCode",OPERATION_SUCCESS);
        map.put("statusMsg", CodeMessage.OPERATION_SUCCESS);
        return JSON.toJSONString(map);
    }

    /**
     * 操作失败返回信息
     */
    protected String createErrorJson(CodeMessage codeMessage) {
        return JSON.toJSONString(putFailedInMap().put("reason",codeMessage));
    }

    /**
     * 系统问题返回
     */
    protected String createSystemErrorJson(CodeMessage codeMessage) {
        Map map = new LinkedHashMap();
        map.put("statusCode",SYSTEM_PROBLEM);
        map.put("statusMsg",codeMessage);
        return JSON.toJSONString(map);
    }
}
