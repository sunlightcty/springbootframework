package scau.cty.base.mybatis;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * 自定义 mapper 父类，注意这个类不要让 mp 扫描到！！
 * Created by SunlightCloud on 2017/7/20.
 */
public interface SuperMapper<T> extends BaseMapper<T> {

    // 这里可以放一些公共的方法
}
