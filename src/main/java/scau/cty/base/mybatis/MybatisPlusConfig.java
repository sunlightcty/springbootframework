package scau.cty.base.mybatis;

import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.incrementer.OracleKeyGenerator;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import scau.cty.base.config.druid.DruidConfig;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by SunlightC on 2017/7/20.
 */
@Configuration
@MapperScan("scau.cty.boot.mapper")
public class MybatisPlusConfig {

    /**
     * mybatis-plus分页插件<br>
     * 文档：http://mp.baomidou.com<br>
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

//    @Autowired
//    private DruidConfig dataSource ;
//
//    public MybatisPlusConfig(){
//        System.out.println("init MyBatisPlusConfig");
//    }
//
//    @Bean
//    public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
//
//        MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
//        sqlSessionFactoryBean.setDataSource(druidConfig.dataSource());
//        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        sqlSessionFactoryBean.setGlobalConfig(globalConfiguration());
//        sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/mapper/*.xml"));
//        sqlSessionFactoryBean.setTypeAliasesPackage("scau.cty.boot.entity");
//
//        //设置 properties
//
//        Properties properties = new Properties();
//        properties.setProperty("dialect","mssql");
//        properties.setProperty("pageSqlId",".*query.*");
//
//        //设置自定义插件
//        Interceptor[] interceptors = new Interceptor[1];
//        sqlSessionFactoryBean.setPlugins(interceptors);
//        return sqlSessionFactoryBean.getObject();
//    }
//
//    @Bean
//    public DataSourceTransactionManager transactionManager() throws Exception {
//        return new DataSourceTransactionManager(druidConfig.dataSource());
//    }
//
//    @Bean
//    public GlobalConfiguration globalConfiguration() {
//        GlobalConfiguration gc = new GlobalConfiguration();
//        gc.setIdType(2);
//        gc.setKeyGenerator(new OracleKeyGenerator());
//        gc.setDbColumnUnderline(true);
//        return gc;
//    }
}
