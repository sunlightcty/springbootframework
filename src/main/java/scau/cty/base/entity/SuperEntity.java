package scau.cty.base.entity;


import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.KeySequence;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import javax.persistence.GeneratedValue;
import java.io.Serializable;

/**
 * 实体父类
 * ActiveRecord实现 继承Model
 * Created by Administrator on 2017/7/20.
 */
@KeySequence("SEQ_TEST")//类注解
public class SuperEntity<T extends Model> extends Model<T> {

    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.ID_WORKER)
    @GeneratedValue(generator = "ID_WORKER")
    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}