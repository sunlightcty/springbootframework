package scau.cty;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import scau.cty.boot.entity.User;
import scau.cty.boot.mapper.UserMapper;
import scau.cty.base.properties.TestProperties;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootApplicationTests {

	@Test
	public void contextLoads() {
	}
	@Autowired
	private TestProperties testProperties;
	@Test
	public void getHello() throws Exception {
		Assert.assertEquals(testProperties.getName(), "SunlightCloud");
		Assert.assertEquals(testProperties.getTitle(), "janer");
	}
	@Autowired
	private JavaMailSender mailSender;
	@Test
	public void sendSimpleMail() throws Exception {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("sunlightcloud663@qq.com");
		message.setTo("1430569666@qq.com");
		message.setSubject("主题：简单邮件");
		message.setText("测试邮件内容");
		mailSender.send(message);
	}


}
